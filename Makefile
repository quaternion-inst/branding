all: logo.png avatar.png favicon.ico

logo.png:
	mkdir -p build
	inkscape --export-filename="build/logo.png" logo.svg -w 2048 -h 2048
	optipng build/logo.png

avatar.png:
	mkdir -p build
	inkscape --export-filename="build/avatar.png" -b "#30323f" logo.svg -w 2048 -h 2048
	optipng build/avatar.png

favicon.ico: avatar.png
	mkdir -p build
	magick convert build/avatar.png -resize 16x16 build/16.png
	magick convert build/avatar.png -resize 32x32 build/32.png
	magick convert build/avatar.png -resize 48x48 build/48.png
	magick convert build/16.png build/32.png build/48.png build/favicon.ico

clean:
	rm build/logo.png
	rm build/avatar.png
	rm build/16.png
	rm build/32.png
	rm build/48.png
	rm build/favicon.ico
