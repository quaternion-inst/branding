# Branding
This repository contains branding materials for the Quaternion Institute.


## Licensing
Unless otherwise stated, everything in this repository is licensed under CC BY-NC-ND.
This means the materials can be shared and downloaded, but not modified or sold.
We are open to variations of the logo being used for related projects, but the changes must be approved beforehand by the project governance.

## Usage
SVG should always be preferred due to scalability and bandwidth savings.
PNGs are used when SVGs can't be used.
For uniformity, PNGs are generated using scripts from SVGs.

### Color palette
The brand colors used are defined in `colors.sass`. 

## Building
Use `make` to build PNGs/ICOs from the SVGs.
You will need `inkscape` and `optipng` installed.
